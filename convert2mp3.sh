#!/usr/bin/bash


dir="$PWD"

# Change this convertedDir to where you want your converted files to be saved in.
convertedDir="${PWD}/test"

for i in *.mp4; do
    track="${i%%.*}"
   ffmpeg -i "$i"  -vn -acodec libmp3lame -ac 2 -ab 160k -ar 48000 "${convertedDir}/${track}".mp3
done

for i in *.webm; do
    track="${i%%.*}"
   ffmpeg -i "$i"  -vn -acodec libmp3lame -ac 2 -ab 160k -ar 48000 "${convertedDir}/${track}".mp3
done

for i in *.flv; do
    track="${i%%.*}"
   ffmpeg -i "$i"  -vn -acodec libmp3lame -ac 2 -ab 160k -ar 48000 "${convertedDir}/${track}".mp3
done

for i in *.mkv; do
    track="${i%%.*}"
   ffmpeg -i "$i"  -vn -acodec libmp3lame -ac 2 -ab 160k -ar 48000 "${convertedDir}/${track}".mp3
done

